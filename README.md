# UA Bootstrap

## End of life (2023-11-15)
As of November 15, 2023, UA Bootstrap, UA Quickstart (Quickstart 1) and all of their associated repositories will no longer supported by the Arizona Digital team.

- Any website still using UA Bootstrap after the end-of-life date should be [updated to use Arizona Bootstrap](https://digital.arizona.edu/arizona-bootstrap/docs/2.0/migration/) instead.
- Any website still using Quickstart 1 after the end-of-life date should be migrated to [Quickstart 2](https://quickstart.arizona.edu) as soon as possible. See the [main UA Quickstart repository](https://bitbucket.org/ua_drupal/ua_quickstart/src/7.x-1.x/README.md) for more information.

## Overview
UA's flavor of the Bootstrap Framework, combining the new UA Brand with the
goodness of Bootstrap 3.


## Using UA Bootstrap

### CDN
Instructions for using the UA Digital CDN to include UA Bootstrap in your project
can be found on the homepage:

[http://uadigital.arizona.edu/ua-bootstrap/](http://uadigital.arizona.edu/ua-bootstrap/)

### Versioning scheme
UA Bootstrap uses the [SemVer](http://semver.org/) scheme to provide informative
version numbers for each release in the form of MAJOR.MINOR.PATCH.

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards-compatible manner, and
3. PATCH version when you make backwards-compatible bug fixes.


## Contributing

### Dependencies
* [Git](https://git-scm.com)
* [Node.js](https://nodejs.org/)
* [Yarn](https://yarnpkg.com/)
* [Gulp](http://gulpjs.com/)

### Initial setup for development in a local machine

***UA Bootstrap build tools are tested to be compatible with the LTS version of Node.js (10.x.x)***

If you need to modify UA Bootstrap in a local development environment, it must
include Node.js. If you have not installed this already,
[Node Version Manager](https://github.com/creationix/nvm) is one way to set it
up on macOS or Linux so that changes between node versions are relatively easy.

If you are contributing your modifications to UA Bootstrap back to the main
repository, you must use the Git source control software. The instructions for
using this with UA Bootstrap are similar to those given for the
[UA QuickStart](https://bitbucket.org/ua_drupal/ua_quickstart) Drupal
distribution, and in particular UA Bootstrap uses the same workflow for
proposing changes through issue branches and pull requests on
[Bitbucket](https://bitbucket.org), but of course referring to
[UA Bootstrap](https://bitbucket.org/uadigital/ua-bootstrap) in place of
QuickStart. If you have permissions to access it, you should work from the main
repository, cloning it to your own environment with the
```
git clone git@bitbucket.org:uadigital/ua-bootstrap.git
```
command. If not, you will need to fork your own copy of the UA Bootstrap
repository on Bitbucket, then work from a local clone of that fork.

The Yarn package manager is required to rebuild UA Bootstrap in your own
environment (to handle the many Node.js dependencies). The default Yarn
installation instructions may be appropriate for an normal user, but for a
developer the
[alternative instructions](https://yarnpkg.com/en/docs/install#alternatives-tab)
may be better.

Currently the Gulp task runner must be available globally before you can build
UA Bootstrap. To set this up, use the `yarn global add` command, something like
```
yarn global add gulp@'~3.9.1'
```
where the `3.9.1` is the current stable Gulp release.

### Initial setup for development using Docker

If you use Docker, the public image `uadigital/ua-bootstrap-node-image` is a
good point of departure. Please specify the tagged version that matches the
version of UA Bootstrap you are working with: this appears at the start of the
bitbucket-piplines.yml file on the UA Bootstrap repository. The Dockerfile for
this image is in its
[own repository](https://bitbucket.org/uadigital/ua-bootstrap-node-image), and
it has Git, the Yarn package manager, and the Gulp task runner pre-installed.
You must specify that you wish to run a command-line shell rather than Node.js,
and if you plan to run the live browser preview you must explicitly publish the
port used for this, something like
```
docker run -t -i  --entrypoint /bin/bash -p 8888:8888 uadigital/ua-bootstrap-node-image:1.3.0
```
However you will need additional setup to share a directory outside the
container for editing the files directly, or for pushing Git changes from the
cloned repository within the container back up to Bitbucket. The usual public
HTTPS remote repository reference should always work within the container
```
git clone https://bitbucket.org/uadigital/ua-bootstrap.git
```
if you do not need to push changes.

### Building
(Items shown `like this` are commands to be run in your terminal.)

1. Clone the Git repository on Bitbucket into your own environment or container
2. Navigate to your cloned repository: `cd ua-bootstrap`
3. If necessary, check out an issue branch: `git checkout` and the branch name
4. Install dependencies (and build): `yarn`
5. Start the development server: `gulp serve`
6. Open [http://localhost:8888](http://localhost:8888) in your browser, and voilà.


### On making edits to UA Bootstrap
UA Bootstrap is entirely based on the Bootstrap Framework and thus is largely
a bunch of overrides. Sometimes Bootstrap doesn't have everything we'd like in
UA Bootstrap, so when adding a feature that is not an override to Vanilla
Bootstrap, place those additions/appendages in `_custom.scss`.

Yarn is responsible for controlling the numerous Node.js packages that help
build UA Bootstrap, so if you change any of these you should make sure Yarn is
tracking the change. The `package.json` file lists the packages and the
acceptable ranges of the versions to install. Different developers (and the
continuous integration systems) might end up with collections of packages that
are all at acceptable versions, but all slightly different, potentially behaving
differently. To keep everyone on the same page, the `yarn.lock` file tells Yarn
to only use one specific version, even if `package.json` allows a range of
versions. If possible, use commands like `yarn add --dev` and `yarn ugrade` to
synchronize the changes both files: full details how to use these are in the
[dependency management documentation](https://yarnpkg.com/en/docs/managing-dependencies).
If you want other people to use your changes, push the modified versions of both
files back up to the repository on Bitbucket.


## Development tips

### Making edits in Chrome persistent
Picking up from the last step, you should now have the Bootstrap documentation running
locally. Now to wire up Chrome DevTools to write changes out to the files you
are serving up. Go back to the browser and fire up Chrome DevTools and head over
to the __Sources__ tab. There should be a __Sources__ pane with a list of
domains where all your assets are being downloaded from. Right click in that pane
and select __Add Folder to Workspace__.

![Add Folder to
Workspace](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-1.png)

Add the directory where the ua-bootstrap repo is located. For me it was on my
Desktop so I added `~/Desktop/ua-bootstrap`. Now the folder is added to your
workspace. Now to link the files to the folder in your workspace. Go back to the
__Sources__ pane and right-click `ua-bootstrap.scss` in the folder titled
`source` under `localhost:8888`. Click __Map to File System Resource...__ and it
should auto-find the file in the added workspace folder. Hit enter and you're
set.

![Map to File System Resource...](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-2.png)

![ua-bootstrap.scss](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-3.png)

One caveat on using Chrome inspector to edit. Chrome does not have the ability
to make edits to the `*.scss` under the __Elements__ tab. You'll have to make
all edits in the __Sources__ tab, which is a bit of a pain. So find the files
you need to override in the __Elements__ tab, but then select the reference link
to bring you to the __Sources__ tab where you can make the edit and save.

![Make edit in Sources Tab](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-4.png)


## Tagging a release

[See instructions on Confluence](https://confluence.arizona.edu/display/UADigital/Releasing+a+New+Version+of+Bootstrap)
