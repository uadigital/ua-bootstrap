module.exports = {
  /* Create Browsersync Server */
  start: (gulp, config) => () => {
    config.bs.init(config.bsConfig)

    gulp.watch(['src/sass/**/**/**', 'src/svg/**', 'src/img/**'], ['sass', 'docs:assets'])
    gulp.watch(['src/js/**'], ['js'])
    gulp.watch(['docs/pug/**/**/**'], ['docs:pug']).on('end', config.bs.reload)
    gulp.watch(['docs/**/**', '!docs/pug/**/**/**'], ['docs:assets']).on('end', config.bs.reload)
  }
}
