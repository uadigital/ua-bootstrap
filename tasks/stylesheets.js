const $ = {
  autoprefixer: require('gulp-autoprefixer'),
  cleanCss: require('gulp-clean-css'),
  rename: require('gulp-rename'),
  replace: require('gulp-replace'),
  sass: require('gulp-sass'),
  svgmin: require('gulp-svgmin')
}

module.exports = {
  /* Minifies built CSS file(s) */
  minify: (gulp, config) => () => {
    return gulp.src(config.buildDir + '/lib/ua-bootstrap.css')
      .pipe($.rename({ suffix: '.min' }))
      .pipe($.cleanCss({processImport: false}))
      .pipe(gulp.dest(config.buildDir + '/lib'))
  },

  /* Compiles SASS styles into single distributable CSS file */
  sass: (gulp, config) => () => {
    return gulp.src(config.srcDir + '/sass/ua-bootstrap.scss')
      .pipe($.sass({
        includePaths: [config.bootstrapDir + '/stylesheets'],
        outputStyle: 'expanded',
        precision: 6,
        sourceComments: true
      }))
      .pipe($.autoprefixer('last 2 versions'))
      .pipe(gulp.dest(config.buildDir + '/lib'))
      .pipe(config.bs.stream())
  },

  /* Removes Glyphicons from the included Bootstrap Sass package */
  removeGlyphicons: (gulp, config) => () => {
    return gulp.src(config.bootstrapDir + '/stylesheets/_bootstrap.scss')
      .pipe($.replace(config.glyphiconsRemove, ''))
      .pipe(gulp.dest(config.bootstrapDir + '/stylesheets/', {overwrite: true}))
  },

  /* Minifies SVG files and copies them to build directory */
  svg: (gulp, config) => () => {
    return gulp.src(config.srcDir + '/svg/*.svg')
      .pipe($.svgmin())
      .pipe(gulp.dest(config.buildDir + '/lib/img/'))
  },

  /* Copies other image files to build directory */
  img: (gulp, config) => () => {
    return gulp.src(config.srcDir + '/img/**')
      .pipe(gulp.dest(config.buildDir + '/lib/img/'))
  }
}
