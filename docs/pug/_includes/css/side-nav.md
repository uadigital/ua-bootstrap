<div id="catstrap-sidebar" data-spy="affix" data-offset-top="303" class="hidden-print hidden-xs hidden-sm affix-top">
    <ul class="nav bs-docs-sidenav">
        <li>
            [Grid system](#grid)
            <ul class="nav">
                <li>[Introduction](#introduction)</li>
                <li>[Grid options](#grid-options)</li>
                <li>[Ex: Stacked-to-horizontal](#example-stacked-to-horizontal)</li>
                <li>[Ex: Fluid container](#example-fluid-container)</li>
                <li>[Ex: Mobile and desktop](#example-mobile-and-desktop)</li>
                <li>[Ex: Mobile, tablet, desktop](#example-mobile-tablet-desktop)</li>
                <li>[Ex: Column wrapping](#example-column-wrapping)</li>
                <li>[Responsive column resets](#responsive-column-resets)</li>
                <li>[Offsetting columns](#offsetting-columns)</li>
                <li>[Nesting columns](#nesting-columns)</li>
                <li>[Column ordering](#column-ordering)</li>
                <li>[Spacing<span class="label label-default">New</span>](#spacing)</li>
                <li>[Horizontal Centering<span class="label label-default">New</span>](#horizontal-centering)</li>
                <li>[Row buffer<span class="label label-warning">Deprecated</span>](#row-buffer)</li>
                <li>[Column buffer<span class="label label-warning">Deprecated</span>](#column-buffer)</li>
            </ul>
        </li>
        <li>
            [Display<span class="label label-default">New</span>](#display)
            <ul class="nav">
                <li>[How it works<span class="label label-default">New</span>](#display-how-it-works)</li>
                <li>[Notation<span class="label label-default">New</span>](#display-notation)</li>
                <li>[Hiding Elements<span class="label label-default">New</span>](#display-hiding-elements)</li>
                <li>[Examples<span class="label label-default">New</span>](#display-examples)</li>
            </ul>
        </li>
        <li>
            [Flex<span class="label label-default">New</span>](#flex)
            <ul class="nav">
                <li>[Enable Flex Behaviors<span class="label label-default">New</span>](#flex-enable-flex-behaviors)</li>
                <li>[Direction<span class="label label-default">New</span>](#flex-direction)</li>
                <li>[Justify Content<span class="label label-default">New</span>](#flex-justify-content)</li>
                <li>[Align Items<span class="label label-default">New</span>](#flex-align-items)</li>
                <li>[Align Self<span class="label label-default">New</span>](#flex-align-self)</li>
                <li>[Auto Margins<span class="label label-default">New</span>](#flex-auto-margins)</li>
                <li>[With Align Items<span class="label label-default">New</span>](#flex-with-align-items)</li>
                <li>[Wrap<span class="label label-default">New</span>](#flex-wrap)</li>
                <li>[Align Content<span class="label label-default">New</span>](#flex-align-content)</li>
                <li>[Flex Within the Grid<span class="label label-default">New</span>](#flex-within-the-grid)</li>
            </ul>
        </li>
        <li>
            [Typography<span class="label label-info">Updated</span>](#typography)
            <ul class="nav">
                <li>[Text Color Classes<span class="label label-default">New</span>](#text-colors)</li>
                <li>[Headings<span class="label label-info">Updated</span>](#headings)</li>
                <li>[Heading Options<span class="label label-default">New</span>](#heading-options)</li>
                <li>[Margin Vertical Align<span class="label label-info">Updated</span>](#margin-vertical-align)</li>
                <li>[Font Size<span class="label label-default">New</span>](#font-size)</li>
                <li>[Body Copy](#body-copy)</li>
                <li>[Lead Body Copy](#lead-body-copy)</li>
                <li>[Inline Text Elements](#inline-text-elements)</li>
                <li>[Alignment Classes](#alignment-classes)</li>
                <li>[Responsive Alignment Classes<span class="label label-default">New</span>](#responsive-alignment-classes)</li>
                <li>[Transformation Classes](#transformation-classes)</li>
                <li>[Lists](#lists)</li>
                <li>[Utility Classes](#utility-classes)</li>
                <li>[Blockquotes<span class="label label-default">New</span>](#blockquotes)</li>
            </ul>
        </li>
        <li>
            [Tables](#tables)
            <ul class="nav">
                <li>[Basic](#basic)</li>
                <li>[Hover Rows](#hover-rows)</li>
                <li>[Responsive Tables](#responsive-tables)</li>
            </ul>
        </li>
        <li>
            [Forms<span class="label label-info">Updated</span>](#forms)
            <ul class="nav">
                <li>[Basic Example<span class="label label-info">Updated</span>](#basic-example)</li>
            </ul>
        </li>
        <li>
            [Buttons](#buttons)
            <ul class="nav">
              <li>[Button tags](#buttons-tags)</li>
              <li>[Options](#buttons-options)</li>
              <li>[Arrow Buttons](#arrow-buttons)</li>
              <li>[Hollow Buttons<span class="label label-default">New</span>](#hollow-buttons)</li>
              <li>[Sizes](#buttons-sizes)</li>
              <li>[Active state](#buttons-active)</li>
              <li>[Disabled state](#buttons-disabled)</li>
            </ul>
        </li>
        <li>
          [External Links<span class="label label-default">New</span>](#external-links)
        </li>
        <li class="back-to-top">[Back to top](#top)</li>
    </ul>
</div>
