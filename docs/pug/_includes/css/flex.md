<a id="flex"></a>
## Flex

<p class="lead">Quickly manage the layout, alignment, and sizing of grid columns, navigation, components, and more with a full suite of responsive flexbox utilities. For more complex implementations, custom CSS may be necessary.</p>

For more information see <a href="https://getbootstrap.com/docs/4.0/utilities/flex/" target="_blank">Bootstrap 4 Flex</a>

Additional information on how flex works can be found on <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">CSS-Tricks' Complete Guide to Flexbox</a>

<div class="callout callout-warning">
    <h4 class="callout-heading"> Compatibility Warning</h4>
    <p>Internet Explorer and Microsoft Edge require that a specific width be defined for flex containers in order to display properly.</p>
</div>

<a id="flex-enable-flex-behaviors"></a>
### Enable Flex Behaviors

Apply [display](#display) utilities to create a flexbox container and transform direct children elements into flex items. Flex containers and items are able to be modified further with additional flex properties.

<div class="example">
    <div class="d-flex p-2 bg-red text-white">I'm a flexbox container!</div>
</div>

```html
<div class="d-flex p-2 bg-red text-white">I'm a flexbox container!</div>
```

<div class="example">
    <div class="d-inline-flex p-2 bg-red text-white">I'm an inline flexbox container!</div>
</div>

```html
<div class="d-inline-flex p-2 bg-red text-white">I'm an inline flexbox container!</div>
```

Responsive variations also exist for `.d-flex` and `.d-inline-flex`.

- `.d-flex`
- `.d-inline-flex`
- `.d-sm-flex`
- `.d-sm-inline-flex`
- `.d-md-flex`
- `.d-md-inline-flex`
- `.d-lg-flex`
- `.d-lg-inline-flex`
- `.d-xl-flex`
- `.d-xl-inline-flex`

<a id="flex-direction"></a>
### Direction

Set the direction of flex items in a flex container with direction utilities. In most cases you can omit the horizontal class here as the browser default is row. However, you may encounter situations where you needed to explicitly set this value (like responsive layouts).

Use `.flex-row` to set a horizontal direction (the browser default), or `.flex-row-reverse` to start the horizontal direction from the opposite side.

<div class="example">
    <div class="d-flex flex-row well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex flex-row-reverse well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
</div>

```html
<div class="d-flex flex-row well">
    <div class="p-2 bg-red text-white">Flex item 1</div>
    <div class="p-2 bg-blue text-white">Flex item 2</div>
    <div class="p-2 bg-red text-white">Flex item 3</div>
</div>
<div class="d-flex flex-row-reverse well">
    <div class="p-2 bg-red text-white">Flex item 1</div>
    <div class="p-2 bg-blue text-white">Flex item 2</div>
    <div class="p-2 bg-red text-white">Flex item 3</div>
</div>
```

Use `.flex-column` to set a vertical direction, or `.flex-column-reverse` to start the vertical direction from the opposite side.

<div class="example">
    <div class="d-flex flex-column">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <br>
    <div class="d-flex flex-column-reverse">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
</div>

```html
<div class="d-flex flex-column">
    <div class="p-2 bg-red text-white">Flex item 1</div>
    <div class="p-2 bg-blue text-white">Flex item 2</div>
    <div class="p-2 bg-red text-white">Flex item 3</div>
</div>
<br>
<div class="d-flex flex-column-reverse">
    <div class="p-2 bg-red text-white">Flex item 1</div>
    <div class="p-2 bg-blue text-white">Flex item 2</div>
    <div class="p-2 bg-red text-white">Flex item 3</div>
</div>
```

Responsive variations also exist for flex-direction.

- `.flex-row`
- `.flex-row-reverse`
- `.flex-column`
- `.flex-column-reverse`
- `.flex-sm-row`
- `.flex-sm-row-reverse`
- `.flex-sm-column`
- `.flex-sm-column-reverse`
- `.flex-md-row`
- `.flex-md-row-reverse`
- `.flex-md-column`
- `.flex-md-column-reverse`
- `.flex-lg-row`
- `.flex-lg-row-reverse`
- `.flex-lg-column`
- `.flex-lg-column-reverse`
- `.flex-xl-row`
- `.flex-xl-row-reverse`
- `.flex-xl-column`
- `.flex-xl-column-reverse`

<a id="flex-justify-content"></a>
### Justify Content

Use `justify-content` utilities on flexbox containers to change the alignment of flex items on the main axis (the x-axis to start, y-axis if `flex-direction: column`). Choose from `start` (browser default), `end`, `center`, `between`, or `around`.

<div class="example">
    <div class="d-flex justify-content-start well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex justify-content-end well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex justify-content-center well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex justify-content-between well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex justify-content-around well">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
</div>

```html
<div class="d-flex justify-content-start">...</div>
<div class="d-flex justify-content-end">...</div>
<div class="d-flex justify-content-center">...</div>
<div class="d-flex justify-content-between">...</div>
<div class="d-flex justify-content-around">...</div>
```

Responsive variations also exist for `justify-content`.

- `.justify-content-start`
- `.justify-content-end`
- `.justify-content-center`
- `.justify-content-between`
- `.justify-content-around`
- `.justify-content-sm-start`
- `.justify-content-sm-end`
- `.justify-content-sm-center`
- `.justify-content-sm-between`
- `.justify-content-sm-around`
- `.justify-content-md-start`
- `.justify-content-md-end`
- `.justify-content-md-center`
- `.justify-content-md-between`
- `.justify-content-md-around`
- `.justify-content-lg-start`
- `.justify-content-lg-end`
- `.justify-content-lg-center`
- `.justify-content-lg-between`
- `.justify-content-lg-around`
- `.justify-content-xl-start`
- `.justify-content-xl-end`
- `.justify-content-xl-center`
- `.justify-content-xl-between`
- `.justify-content-xl-around`

<a id="flex-align-items"></a>
### Align Items

Use `align-items` utilities on flexbox containers to change the alignment of flex items on the cross axis (the y-axis to start, x-axis if `flex-direction: column`). Choose from `start`, `end`, `center`, `baseline`, or `stretch` (browser default).

<div class="example">
    <div class="d-flex align-items-start bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex align-items-end bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex align-items-center bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex align-items-baseline bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex align-items-stretch bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue text-white">Flex item 2</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
</div>

```html
<div class="d-flex align-items-start bg-cool-gray mb-3" style="height: 100px">...</div>
<div class="d-flex align-items-end bg-cool-gray mb-3" style="height: 100px">...</div>
<div class="d-flex align-items-center bg-cool-gray mb-3" style="height: 100px">...</div>
<div class="d-flex align-items-baseline bg-cool-gray mb-3" style="height: 100px">...</div>
<div class="d-flex align-items-stretch bg-cool-gray mb-3" style="height: 100px">...</div>
```

Responsive variations also exist for `align-items`.

- `.align-items-start`
- `.align-items-end`
- `.align-items-center`
- `.align-items-baseline`
- `.align-items-stretch`
- `.align-items-sm-start`
- `.align-items-sm-end`
- `.align-items-sm-center`
- `.align-items-sm-baseline`
- `.align-items-sm-stretch`
- `.align-items-md-start`
- `.align-items-md-end`
- `.align-items-md-center`
- `.align-items-md-baseline`
- `.align-items-md-stretch`
- `.align-items-lg-start`
- `.align-items-lg-end`
- `.align-items-lg-center`
- `.align-items-lg-baseline`
- `.align-items-lg-stretch`
- `.align-items-xl-start`
- `.align-items-xl-end`
- `.align-items-xl-center`
- `.align-items-xl-baseline`
- `.align-items-xl-stretch`

<a id="flex-align-self"></a>
### Align Self

Use `align-self` utilities on flexbox items to individually change their alignment on the cross axis (the y-axis to start, x-axis if `flex-direction: column`). Choose from the same options as `align-items`: `start`, `end`, `center`, `baseline`, or `stretch` (browser default).

<div class="example">
    <div class="d-flex bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue align-self-start text-white">Aligned Self Start</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue align-self-end  text-white">Aligned Self End</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue align-self-center text-white">Aligned Self Center</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue align-self-baseline text-white">Aligned Self Baseline</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
    <div class="d-flex align-self-stretch bg-cool-gray mb-3" style="height: 100px">
        <div class="p-2 bg-red text-white">Flex item 1</div>
        <div class="p-2 bg-blue align-self-stretch text-white">Aligned Self Stretch</div>
        <div class="p-2 bg-red text-white">Flex item 3</div>
    </div>
</div>

```html
<div class="align-self-start">Aligned flex item</div>
<div class="align-self-end">Aligned flex item</div>
<div class="align-self-center">Aligned flex item</div>
<div class="align-self-baseline">Aligned flex item</div>
<div class="align-self-stretch">Aligned flex item</div>
```

Responsive variations also exist for `align-self`.

- `.align-self-start`
- `.align-self-end`
- `.align-self-center`
- `.align-self-baseline`
- `.align-self-stretch`
- `.align-self-sm-start`
- `.align-self-sm-end`
- `.align-self-sm-center`
- `.align-self-sm-baseline`
- `.align-self-sm-stretch`
- `.align-self-md-start`
- `.align-self-md-end`
- `.align-self-md-center`
- `.align-self-md-baseline`
- `.align-self-md-stretch`
- `.align-self-lg-start`
- `.align-self-lg-end`
- `.align-self-lg-center`
- `.align-self-lg-baseline`
- `.align-self-lg-stretch`
- `.align-self-xl-start`
- `.align-self-xl-end`
- `.align-self-xl-center`
- `.align-self-xl-baseline`
- `.align-self-xl-stretch`

<a id="flex-auto-margins"></a>
### Auto Margins

Flexbox can do some pretty awesome things when you mix flex alignments with auto margins. Shown below are three examples of controlling flex items via auto margins: default (no auto margin), pushing two items to the right (`.mr-auto`), and pushing two items to the left (`.ml-auto`).

<div class="callout callout-danger">
    <b>Unfortunately, IE10 and IE11 do not properly support auto margins on flex items whose parent has a non-default</b> `justify-content` <b>value. [See this StackOverflow answer](https://stackoverflow.com/a/37535548) for more details.</b>
</div>

<div class="example">
    <div class="d-flex well">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
    <div class="d-flex well">
        <div class="mr-auto p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
    <div class="d-flex well">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="ml-auto p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex">
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
</div>

<div class="d-flex">
  <div class="mr-auto p-2">Flex item</div>
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
</div>

<div class="d-flex">
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
  <div class="ml-auto p-2">Flex item</div>
</div>
```

<a id="flex-with-align-items"></a>
### With Align Items

Vertically move one flex item to the top or bottom of a container by mixing `align-items`, `flex-direction: column`, and `margin-top: auto` or `margin-bottom: auto`.

<div class="example">
    <div class="d-flex align-items-start flex-column bg-cool-gray mb-3" style="height: 200px;">
        <div class="mb-auto p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
    <div class="d-flex align-items-end flex-column bg-cool-gray mb-3" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="mt-auto p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-items-start flex-column" style="height: 200px;">
  <div class="mb-auto p-2">Flex item</div>
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
</div>

<div class="d-flex align-items-end flex-column" style="height: 200px;">
  <div class="p-2">Flex item</div>
  <div class="p-2">Flex item</div>
  <div class="mt-auto p-2">Flex item</div>
</div>
```

<a id="flex-wrap"></a>
### Wrap

Change how flex items wrap in a flex container. Choose from no wrapping at all (the browser default) with .`flex-nowrap`, wrapping with `.flex-wrap`, or reverse wrapping with `.flex-wrap-reverse`.

<div class="example">
    <div class="d-flex flex-nowrap bg-cool-gray" style="width: 8rem">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex flex-nowrap">
  ...
</div>
```

<div class="example">
    <div class="d-flex flex-wrap bg-cool-gray">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex flex-wrap">
  ...
</div>
```

<div class="example">
    <div class="d-flex flex-wrap-reverse bg-cool-gray">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex flex-wrap-reverse">
  ...
</div>
```

Responsive variations also exist for `flex-wrap`.

- `.flex-nowrap`
- `.flex-wrap`
- `.flex-wrap-reverse`
- `.flex-sm-nowrap`
- `.flex-sm-wrap`
- `.flex-sm-wrap-reverse`
- `.flex-md-nowrap`
- `.flex-md-wrap`
- `.flex-md-wrap-reverse`
- `.flex-lg-nowrap`
- `.flex-lg-wrap`
- `.flex-lg-wrap-reverse`
- `.flex-xl-nowrap`
- `.flex-xl-wrap`
- `.flex-xl-wrap-reverse`

<a id="flex-align-content"></a>
### Align Content

Use `align-content` utilities on flexbox containers to align flex items together on the cross axis. Choose from `start` (browser default), `end`, `center`, `between`, `around`, or `stretch`. To demonstrate these utilities, we’ve enforced `flex-wrap: wrap` and increased the number of flex items.

<div class="callout callout-warning"><b>Heads up! This property has no effect on single rows of flex items.</b></div>

<div class="example">
    <div class="d-flex align-content-start flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-start flex-wrap">
  ...
</div>
```

<div class="example">
    <div class="d-flex align-content-end flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-end flex-wrap">...</div>
```

<div class="example">
    <div class="d-flex align-content-center flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-center flex-wrap">...</div>
```

<div class="example">
    <div class="d-flex align-content-between flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-between flex-wrap">...</div>
```

<div class="example">
    <div class="d-flex align-content-around flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-around flex-wrap">...</div>
```

<div class="example">
    <div class="d-flex align-content-stretch flex-wrap bg-cool-gray" style="height: 200px;">
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
        <div class="p-2 bg-blue text-white">Flex item</div>
        <div class="p-2 bg-red text-white">Flex item</div>
    </div>
</div>

```html
<div class="d-flex align-content-stretch flex-wrap">...</div>
```

Responsive variations also exist for `align-content`.

- `.align-content-start`
- `.align-content-end`
- `.align-content-center`
- `.align-content-around`
- `.align-content-stretch`
- `.align-content-sm-start`
- `.align-content-sm-end`
- `.align-content-sm-center`
- `.align-content-sm-around`
- `.align-content-sm-stretch`
- `.align-content-md-start`
- `.align-content-md-end`
- `.align-content-md-center`
- `.align-content-md-around`
- `.align-content-md-stretch`
- `.align-content-lg-start`
- `.align-content-lg-end`
- `.align-content-lg-center`
- `.align-content-lg-around`
- `.align-content-lg-stretch`
- `.align-content-xl-start`
- `.align-content-xl-end`
- `.align-content-xl-center`
- `.align-content-xl-around`
- `.align-content-xl-stretch`

<a id="flex-within-the-grid"></a>
### Flex Within the Grid

Apply [display](#display) and [flex](#flex-enable-flex-behaviors) utilities to create a flexbox container and transform direct children elements into flex items. Flex containers and items are able to be modified further with additional flex properties.

Note: When using display and flex utilities with columns, the xs column size must be set to work as intended on mobile.

<div class="example">
    <div class="row d-flex flex-wrap">
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block bg-blue text-white height-100">
                    I'm a flexbox container!
                </div>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block height-100">
                    I'm a flexbox container with a lot of text that makes me very long compared to the other flexbox containers in this grid to show you how all the heights are the same!
                </div>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block bg-blue text-white height-100 d-flex flex-column">
                    I'm a flexbox container with a button aligned to the bottom!
                    <a class="btn btn-hollow-reverse mt-auto" href="#flex-within-the-grid">Button</a>
                </div>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block height-100">
                    <h3>Sample Text Heading</h3>
                    <p>I'm a flexbox container with a lot of text that makes me very long compared to the other flexbox containers!</p>
                </div>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block bg-chili text-white height-100 d-flex flex-column">
                    <span class="my-auto text-center">I'm a flexbox container with my text aligned to the middle!</span>
                </div>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-6 col-md-4">
            <div class="content height-100 pb-4">
                <div class="card card-block height-100 d-flex flex-column">
                    <span class="mt-auto">I'm a flexbox container with my text aligned to the bottom!</span>
                </div>
            </div>
        </div>
    </div>
</div>

```html
<div class="row d-flex flex-wrap">
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block bg-blue text-white height-100">
                I'm a flexbox container!
            </div>
        </div>
    </div>
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block height-100">
                I'm a flexbox container with a lot of text that makes me very long compared to the other flexbox containers in this grid to show you how all the heights are the same!
            </div>
        </div>
    </div>
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block bg-blue text-white height-100 d-flex flex-column">
                I'm a flexbox container with a button aligned to the bottom!
                <a class="btn btn-hollow-reverse mt-auto" href="#flex-within-the-grid">Button</a>
            </div>
        </div>
    </div>
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block height-100">
                <h3>Sample Text Heading</h3>
                <p>I'm a flexbox container with a lot of text that makes me very long compared to the other flexbox containers!</p>
            </div>
        </div>
    </div>
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block bg-chili text-white height-100 d-flex flex-column">
                <span class="my-auto text-center">I'm a flexbox container with my text aligned to the middle!</span>
            </div>
        </div>
    </div>
    <div class="col col-xs-12 col-sm-6 col-md-4">
        <div class="content height-100 pb-4">
            <div class="card card-block height-100 d-flex flex-column">
                <span class="mt-auto">I'm a flexbox container with my text aligned to the bottom!</span>
            </div>
        </div>
    </div>
</div>
```