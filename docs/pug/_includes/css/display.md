<a id="display"></a>
## Display

<p class="lead">
Quickly and responsively toggle the display value of components and more with our display utilities. Includes support for some of the more common values, as well as some extras for controlling display when printing.
</p>

For more information see: <a href="https://getbootstrap.com/docs/4.0/utilities/display/" target="_blank">Bootstrap 4 Display</a>

<a id="display-how-it-works"></a>
### How it works

Change the value of the display property with our responsive display utility classes. We purposely support only a subset of all possible values for display. Classes can be combined for various effects as you need.

<a id="display-notation"></a>
### Notation

Display utility classes that apply to all breakpoints, from xs to xl, have no breakpoint abbreviation in them. This is because those classes are applied from min-width: 0; and up, and thus are not bound by a media query. The remaining breakpoints, however, do include a breakpoint abbreviation.

As such, the classes are named using the format:

`.d-{value}` for `xs`
`.d-{breakpoint}-{value}` for `sm`, `md`, `lg`, and `xl`.

Where value is one of:

- `none`
- `inline`
- `inline-block`
- `block`
- `table`
- `table-cell`
- `table-row`
- `flex`
- `inline-flex`

The media queries effect screen widths with the given breakpoint or larger. For example, `.d-lg-none` sets `display: none;` on both `lg` and `xl` screens.

<a id="display-hiding-elements"></a>
### Hiding Elements

For faster mobile-friendly development, use responsive display classes for showing and hiding elements by device. Avoid creating entirely different versions of the same site, instead hide element responsively for each screen size.

To hide elements simply use the `.d-none` class or one of the `.d-{sm,md,lg,xl}-none` classes for any responsive screen variation.

To show an element only on a given interval of screen sizes you can combine one `.d-*-none` class with a `.d-*-*`class, for example `.d-none` `.d-md-block` `.d-xl-none` will hide the element for all screen sizes except on medium and large devices.

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Screen Size</th>
                <th>Class</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Hidden on all</td>
                <td>`.d-none`</td>
            </tr>
            <tr>
                <td>Hidden only on xs</td>
                <td>`.d-none .d-sm-block`</td>
            </tr>
            <tr>
                <td>Hidden only on sm</td>
                <td>`.d-sm-none .d-md-block`</td>
            </tr>
            <tr>
                <td>Hidden only on md</td>
                <td>`.d-md-none .d-lg-block`</td>
            </tr>
            <tr>
                <td>Hidden only on lg</td>
                <td>`.d-lg-none .d-xl-block`</td>
            </tr>
            <tr>
                <td>Hidden only on xl</td>
                <td>`.d-xl-none`</td>
            </tr>
            <tr>
                <td>Visible on all</td>
                <td>`.d-block`</td>
            </tr>
            <tr>
                <td>Visible only on xs</td>
                <td>`.d-block .d-sm-none`</td>
            </tr>
            <tr>
                <td>Visible only on sm</td>
                <td>`.d-none .d-sm-block .d-md-none`</td>
            </tr>
            <tr>
                <td>Visible only on md</td>
                <td>`.d-none .d-md-block .d-lg-none`</td>
            </tr>
            <tr>
                <td>Visible only on lg</td>
                <td>`.d-none .d-lg-block .d-xl-none`</td>
            </tr>
            <tr>
                <td>Visible only on xl</td>
                <td>`.d-none .d-xl-block`</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="example">
    <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
    <div class="d-lg-none">hide on screens wider than lg</div>
    <div class="d-none d-lg-block">hide on screens smaller than lg</div>
</div>
```html
<div class="d-lg-none">hide on screens wider than lg</div>
<div class="d-none d-lg-block">hide on screens smaller than lg</div>
```

<a id="display-examples"></a>
### Examples

<div class="example">
    <p class="example-label text-size-h3"><span class="label label-info">d-inline</span></p>
    <div class="d-inline p-2 bg-red text-white">d-inline</div>
    <div class="d-inline p-2 bg-blue text-white">d-inline</div>
</div>
```html
<div class="d-inline p-2 bg-red text-white">d-inline</div>
<div class="d-inline p-2 bg-blue text-white">d-inline</div>
```

<div class="example">
    <p class="example-label text-size-h3"><span class="label label-info">d-block</span></p>
    <span class="d-block p-2 bg-red text-white">d-block</span>
    <span class="d-block p-2 bg-blue text-white">d-block</span>
</div>
```html
<span class="d-block p-2 bg-red text-white">d-block</span>
<span class="d-block p-2 bg-blue text-white">d-block</span>
```

