  <h2 id="external-links">External Links</h2>
  <p>If you would like to style all external links on a webpage, we provide a simple solution. Add `class='external-links ua-brand-icons'` to the document `<html>` element. Then just make sure that all external links open up in a new tab using `target='_blank'`.  
  <div class="example" data-example-id="large-well">
    <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
      <h4>Navigation</h4>
      <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="text">MAIN MENU</span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="https://www.uanews.arizona.edu" target="_blank"><span>External link</a></li>
            <li class="active"><a href="#">Link<span class="sr-only">(current)</span></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="https://www.uanews.arizona.edu" target="_blank">External link</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <h4>Body</h4>
    <div>
      <h2><a href="https://uadigital.arizona.edu/ua-bootstrap/" target="_blank">H2 Link</a> with text</h2>
      <ul class="triangle">
        <li><a href="https://uadigital.arizona.edu/ua-bootstrap/">Link</a></li>
        <li><a href="https://uanews.arizona.edu" target="_blank">UA News External Link</a></li>
      </ul>
      <div class="btn btn-default" target="_blank">External Link Button</div>
    </div>
  </div>

  ```html
  <html class="external-links ua-brand-icons">
  ...
    <ul class="nav navbar-nav">
      <li><a href="https://www.uanews.arizona.edu" target="_blank"><span>External link</a></li>
      <li class="active"><a href="#">Link<span class="sr-only">(current)</span></a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="https://uanews.arizona.edu" target="_blank">External link</a></li>
        </ul>
      </li>
    </ul>
  ...
    <h2><a href="https://uadigital.arizona.edu/ua-bootstrap/" target="_blank">H2 Link</a> with text</h2>
    <ul class="triangle">
      <li><a href="https://uadigital.arizona.edu/ua-bootstrap/">Link</a></li>
      <li><a href="https://uanews.arizona.edu" target="_blank">UA News External Link</a></li>
    </ul>
    <div class="btn btn-default" target="_blank">External Link Button</div>
  ...
  </html>
  ```
  <div class="bs-callout bs-callout-warning">
    <h4>UA-brand-icons class for external links</h4>
    <p>External links icon is provided by the `ua-brand-icons` class on the html element of the document. If for any reason you do not wish to use `ua-brand-icons` and remove the class, there is a fallback in css that will still place the external links triangle icon.</p>
  </div>

  <div>
  <h3>External Link Without `ua-brand-icons` class</h3>
    <p>This is what your external links will look like without the `ua-brand-icons` class</p>
    <div class="bs-callout bs-callout-warning">
    <h4>Note the difference with the colored text and the external link icon</h4>
  </div>
  </div>

  <div class="example override-ua-brand-icons" data-example-id="external links">
    <p class="example-label text-size-h3"><span class="label label-info">Example <small>in an iframe without `.ua-brand-icons`</em></small></span></p>
    <iframe title="Page without ua-brand-icons class" width="100%" height="365" frameborder="0" scrolling="no"
    src="./examples/without-brand-icons.html"></iframe>
  </div>

  ```html
  <html class="external-links">
  ...
  <a class="card card-landing-grid remove-external-link-icon" href="https://www.arizona.edu" target="_blank">
    <h2 class="h3 text-uppercase text-blue30w">Ipsum Corporate</h2>
    <p class="card-text">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</p>
    <p class="margin-align-bottom text-cactus pseudo-link"><strong><span>How to Apply</span></strong></p>
  </a>
  ```

  <h3>External links icon within a linked area</h3>
  <div>
  <p> In order to display the external link icon for a link within a clickable area for example a clickable card, you have to:
    <ol>
      <li><p>Add this class to your `<a>` element: `class='remove-external-link-icon'`</p>
          <p>This is for removing an extra external links icon that shows up due to the card being a link as well as a link inside the card.</p>
      </li>
      <li>
        <p>Add this class to your element that wraps the `<span>` element for the external link: `class='pseudo-link'`</p>
        <p>This is to target the specific element that will have the external links icon. For the external links icon color to be inherited by the font color, you must have a parent and child relationship present in your markup. For example, `<p class="pseudo-link"><span>pseudo link text</span></p>` where the `<p>` tag is the parent and the `<span>` tag is the child.</p>
      </li>
    </ol>
  </p>
  </div>

  <div class="example" data-example-id="external links">
    <p class="example-label text-size-h3"><span class="label label-info">Example <small>in an iframe without `.ua-brand-icons`</em></small></span></p>
    <iframe title="Page without ua-brand-icons class" width="100%" height="365" frameborder="0" scrolling="no"
    src="./examples/without-brand-icons-linked-area.html"></iframe>
  </div>

  ```html
  <a class="card card-landing-grid remove-external-link-icon" href="https://www.arizona.edu" target="_blank">
    <h2 class="h3 text-uppercase text-blue30w">Ipsum Corporate</h2>
    <p class="card-text">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</p>
    <p class="card-link margin-align-bottom pseudo-link"><span>Ipsum Corporate</span></p>
  </a>
  ...
  <a class="card card-landing-grid remove-external-link-icon" href="https://www.arizona.edu" target="_blank">
    <h2 class="h3 text-uppercase text-blue30w">Ipsum Corporate</h2>
    <p class="card-text">Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition.</p>
    <p class="margin-align-bottom text-cactus pseudo-link"><strong><span>How to Apply</span></strong></p>
  </a>
  ```

  <div>
    <h3>External Link Utility Class</h3>
    <p>If you would like to only use a single class for specific external links, you can add an external-link icon by using `.external`. </p>
    <p>You can add `.external-blue` for a blue version. </p>
  </div>

  <div class="bs-callout bs-callout-danger">
    <h4>`.ext` class has been replaced with `.external`</h4>
    <p>The `.ext` class was replaced in UA Bootstrap in order to not compete with the external link module in Drupal which adds the `.ext` class and the ability to style all external links with an icon.</p>
  </div>

  <div class="example" data-example-id="large-well">
    <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
    <p><a href="#" class="external">External link with the default icon.</a></p>
    <p><a href="#" class="external external-blue">External link with an alternate blue icon.</a></p>
  </div>

  ```html
    <a href="#" class="external">External link with the default icon.</a>
    <a href="#" class="external external-blue">External link with an alternate blue icon.</a>
  ```