## Want to Contribute?

<p class="lead">Any release of UA Bootstrap is tested and ready to use. But improvements to the framework are in everyone's best interest.</p>

<p>A team of web-focused volunteers known as Arizona Digital meets weekly to build and
test products like UA Bootstrap and <a href="https://quickstart.arizona.edu" target="_blank">UA
Quickstart</a>. We're also working on next generation versions: <a href="https://bitbucket.org/uadigital/arizona-bootstrap/src/master/" target="_blank">Arizona Bootstrap</a> (based on Bootstrap 4) and Arizona Quickstart (Drupal 8).</p>

### **If you want to get involved, you can:**

<ul>
    <li><a href="https://quickstart.arizona.edu/join-us-on-slack" target="_blank">Request access</a> to join the discussion on Slack</li>
	<li>Participate in Friday meetings and Wednesday pull request review sessions (<a href="https://uarizona.slack.com/messages/C0SAACCMS" target="_blank">ask for more details in Slack</a>)</li>
	<li>Submit pull requests on <a href="https://bitbucket.org/uadigital/ua-bootstrap/src" target="_blank">Bitbucket</a></li>
</ul>

<p>Questions, bug reports or suggestions can also be emailed to <a href="mailto:uadigital@email.arizona.edu">uadigital@email.arizona.edu</a> or (preferably!) <a href="
https://uarizona.slack.com/messages/C0SAC323G" target="_blank">reported on Slack</a>.</p>