<h1 id="wells">Wells</h1>

<h3>Default well</h3>
<p>Use the well as a simple effect on an element to give it an inset effect.</p>
<div class="example" data-example-id="default-well">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="well">
    Look, I'm in a well!
  </div>
</div>

```html
<div class="well">...</div>
```

<h3>Hollow Well</h3>
<p>Alternately, you can add ```.well-hollow```. </p>
<div class="example" data-example-id="default-well">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="well well-hollow">
    Look, I'm in a hollow well!
  </div>
</div>

```html
<div class="well well-hollow">...</div>
```

<h3>Optional classes</h3>
<p>Control padding and rounded corners with two optional modifier classes.</p>
<div class="example" data-example-id="large-well">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="well well-lg">
    Look, I'm in a large well!
  </div>
</div>

```html
<div class="well well-lg">...</div>
```

<div class="example" data-example-id="small-well">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="well well-hollow well-sm">
    Look, I'm in a small well!
  </div>
</div>

```html
<div class="well well-hollow well-sm">...</div>
```

